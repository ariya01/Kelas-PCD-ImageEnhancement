from PIL import Image

im = Image.open('boat_850.jpg')
rgb_im = im.convert('RGB')
width, height = im.size

for w in range(width):
    for h in range(height):
        color = rgb_im.getpixel((w, h))
        red = 255-color[0]
        green = 255-color[1]
        blue = 255-color[2]
        rgb_im.putpixel((w,h),(red,green,blue))
rgb_im.show()
rgb_im.save('negative.jpg')
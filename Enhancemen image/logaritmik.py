from PIL import Image
import math

im = Image.open('boat_850.jpg')
gray = im.convert('L')
width, height = im.size
c=100

for w in range(width):
    for h in range(height):
        color = gray.getpixel((w, h))
        color = c* math.log(1+color,10)
        gray.putpixel((w,h),int(color))

gray.show()
gray.save('loga.jpg')
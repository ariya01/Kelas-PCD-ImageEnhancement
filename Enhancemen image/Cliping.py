from PIL import Image

im = Image.open('boat_850.jpg')
gray = im.convert('L')
width, height = im.size
# gray = Image.new('L', (width, height))
a=50
b=150
beta =2
# for w in range(width):
#     for h in range(height):
#         r,g,b = rgb_im.getpixel((w, h))
#         red = (r*0.299);
#         green = (g*0.587);
#         blue = (b*0.114);
#         # print int(red+green+blue)
#         # print red
#         gray.putpixel((w,h),int(red+green+blue))

for w in range(width):
    for h in range(height):
        color = gray.getpixel((w, h))
        if 0<=color<a:
            color =0
        elif a<=color<b:
            color = beta*(color-a)
        elif b<=color<256:
            color = beta* (b-a);
        gray.putpixel((w,h),int(color))

gray.show()
gray.save('cliping.jpg')
from PIL import Image

im = Image.open('burung.jpg')
rgb_im = im.convert('RGB')
width, height = im.size
gray = Image.new('L', (width, height))
for w in range(width):
    for h in range(height):
        r,g,b = rgb_im.getpixel((w, h))
        red = (r*0.299);
        green = (g*0.587);
        blue = (b*0.114);
        # print int(red+green+blue)
        # print red
        gray.putpixel((w,h),int(red+green+blue))

gray.show()
gray.save('grey.jpg')
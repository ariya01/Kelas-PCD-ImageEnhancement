from PIL import Image
from collections import Counter
import matplotlib.pyplot as plt
im = Image.open('1.jpg')
gray = im.convert('L')
width, height = im.size

array= list()
for w in range(width):
    for h in range(height):
        color = gray.getpixel((w, h))
        array.append(color)

jumlah = Counter(array)
soreted =list()
pixel = len(array)
graf2=list()
for x in range(256):
    # print repr(x)+ "="+ repr(jumlah[x])
    soreted.append([x,jumlah[x]])
    graf2.append(jumlah[x])

sekarang=0
jadi= list()

for x in range(256):
    sekarang += soreted[x][1]
    hasil = float(sekarang)/pixel*256
    jadi.append([x,hasil])
fix = list()
for w in range(width):
    for h in range(height):
        color = gray.getpixel((w,h))
        color=jadi[color][1]
        gray.putpixel((w,h),int(color))
        fix.append(int(color))

akhirnya = Counter(fix)
graf = list()
for x in range(256):
    graf.append(akhirnya[x])

# print graf
gray.show()
gray.save('histo.jpg')
# # print fix
# plt.plot(graf2)
# # plt.ylabel('some numbers')
# plt.show()
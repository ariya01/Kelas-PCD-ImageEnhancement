from PIL import Image

im = Image.open('boat_850.jpg')
gray = im.convert('L')
width, height = im.size
# gray = Image.new('L', (width, height))
a=50
b=150
beta =2
alfa = 0.2
gama = 1
ya = 30
yb = 200

for w in range(width):
    for h in range(height):
        color = gray.getpixel((w, h))
        if 0<=color<a:
            color = alfa*color
        elif a<=color<b:
            color = beta*(color-a)+ya
        elif b<=color<256:
            color = gama*(color-b)+yb
        gray.putpixel((w,h),int(color))

gray.show()
gray.save('contras.jpg')